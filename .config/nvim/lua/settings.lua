vim.o.syntax = true
vim.o.number = true
vim.o.ignore = true
vim.o.smartcase = true
vim.o.hlsearch = false
vim.o.breakindent = true
vim.o.numberwidth = 1
vim.o.relativenumber = false
vim.o.cindent = true
vim.o.autoindent = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = false
vim.o.clipboard = "unnamedplus"
vim.o.scrolloff = 10

vim.o.termguicolors = true
vim.cmd('colorscheme oceanic-primal')
